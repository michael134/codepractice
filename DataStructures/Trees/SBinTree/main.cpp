#include <iostream>
#include "header/SBinTree.h"

using namespace std;

int main(int argc ,char** argv)
{
    bool empty;
    SBinTree<int> sbt;
    cout<<"Tree Init Passed\n";
    empty = sbt.empty();
    cout<<"Empty Test New Object Value: " << empty<<endl;
    sbt.insert(6);
    sbt.insert(4);
    sbt.insert(2);
    sbt.insert(7);
    sbt.insert(5);
    sbt.insert(8);
    sbt.insert(9);
    cout <<"Insert Tests Passed\n";

    empty = sbt.empty();
    cout<<"Empty Test After Insert Value: " << empty<<endl;

    sbt.remove(9);//remove max
    
    int min = sbt.find_min(); //test min
    int max = sbt.find_max(); //test max
    cout << "Min Max Test for {6 4 2 7 5 8 9} MIN:" <<min  <<" MAX:" <<max <<endl; 


    SBinTree<int> sbt2 = sbt; //call the assignment operator and copy constructor
    cout<< "Assignment Test Passed\n";

    sbt.empty_tree(); //clean out the tree
    cout<<"Clear Tree Passed\n";
    empty = sbt.empty(); //check if empty works on a cleared out tree

    sbt= sbt2;
    sbt.remove(4);
    sbt.remove(2);
    sbt.remove(7);
    sbt.remove(5);
    sbt.remove(8);
    
    cout<<"Remove Test Passed\n";

    empty = sbt.empty(); //check if empty works on a cleared out tree

    sbt.remove(6);
    empty = sbt.empty(); //check if empty works on a cleared out tree  

    return 0;
}




