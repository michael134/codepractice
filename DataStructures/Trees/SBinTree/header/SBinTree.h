#ifndef SBINTREE_H
#define SBINTREE_H
#include <iostream>
#include <ostream>


//*************************************************************************
// SBINTREE_H
//Binary Search Tree
//By: Michael Carey
//Date: Feb/28/2020
//Purpose:
//Series of linked nodes
//root_ represents the base node of the structure
//each node points to only 2 other node children left_ and right_
//
//         () root_
//  left_ /   \ right_
//       ()    ()<--------children nodes
//      /  \   / \
//     ()  () ()  ()<-----grand-children nodes
//*************************************************************************

template <typename obj>
class SBinTree
{
public:
    //ctor copy ctor and dtor
    SBinTree  (                   );
    SBinTree  (const SBinTree& rhs);
    ~SBinTree (                   );   
    //public facing functions
    const obj& find_min   (                     )const;
    const obj& find_max   (                     )const;
    bool       contains   (const obj& item      )const;
    bool       empty      (                     )const{return root_ == 0;}
    void       print_tree (std::ostream & out   )const;
    void       empty_tree (                     );
    void       insert     (const obj& item      );
    void       remove     (const obj& item      );
    SBinTree&  operator=  (const SBinTree& rhs  );

private:
    //module that contains data and
    //pointers to children nodes left_ and right_
    //each node can only have 0-2 children
    struct node
    {
        obj data_;                                        //node's stored data
        node* left_;                                      //pointer to left child node
        node* right_;                                     //pointer to right child node

        node(const obj & data , node* left, node* right): //node ctor
            data_(data),left_(left),right_(right){}
    };
    //*************************************************************************
    node* root_; //pointer to the first node in the tree (root)
    //                                                   /    \
    //                                               (child)  (child)
    //*************************************************************************

    //private functions only to be called internal to the class
    void  insert     (node* & node ,const obj& item           );
    void  remove     (node* & node ,const obj& item           );
    void  empty_tree (node* & node                           );
    void  print_tree (node*   node ,std::ostream& out         )const;
    bool  contains   (node*   node ,const obj& item           )const;
    node* find_min   (node*   node                            )const;
    node* find_max   (node*   node                            )const;
    node* clone      (node*   node                            )const; 
};

//function definitions for SBINTREE_H
template <typename obj>
SBinTree<obj>::SBinTree()
{
    root_ = nullptr; //init the root node
   
}

template <typename obj>
SBinTree<obj>::SBinTree(const SBinTree& rhs)
{
    root_ = clone(rhs.root_);
}

template <typename obj>
SBinTree<obj>::~SBinTree()
{
    empty_tree();
}

template <typename obj>
typename SBinTree<obj>::node* SBinTree<obj>::clone(SBinTree<obj>::node* node) const
{

    if(node == nullptr)//stop
    {
        return nullptr;
    }
    else //return recursive links to left and right tree
    {
        return new SBinTree<obj>::node(node->data_,clone(node->left_),clone(node->right_));
    }
}

template <typename obj>
void SBinTree<obj>::insert(SBinTree<obj>::node* & node, const obj& item)
{
    if(node == nullptr){                                //check if the node points to null therefore its uninitialized
        node = new SBinTree<obj>::node(item,nullptr,nullptr); //build a new empty node with the data inserted
    }
    else if(item < node->data_){                  //else if the item is < the current node's item
        insert(node->left_,item);                 //insert into left node
    }
    else if(item > node->data_){                  //else if the item is > the current nodes item
        insert(node->right_,item);                //insert into right node
    }
    else{                                         //duplicate case
        return;                                   //do nothing
    }
}

template <typename obj>
void SBinTree<obj>::insert(const obj& item)
{
   insert(root_,item);
}

template <typename obj>
void SBinTree<obj>::empty_tree(SBinTree<obj>::node* & node)
{
    if (node != 0)//if the current node isnt null
    {
        empty_tree(node->left_);//recursively remove left tree
        empty_tree(node->right_);//recursively remove right tree
        delete node;
    }
    node = 0;//set current node to null
}

template <typename obj>
void SBinTree<obj>::empty_tree()
{
    empty_tree(root_); //call private function to reclaim tree resources
}

template <typename obj>
void SBinTree<obj>::remove(SBinTree<obj>::node* &node, const obj& item)
{
    if(node == nullptr)
    {
        return;
    }
    else if(item < node->data_)// go left
    {
        remove(node->left_,item);
    }
    else if(item > node->data_)//go right
    {
        remove(node->right_,item);
    }
    else if(node->left_!= nullptr && node->right_ != nullptr)// node has 2 children
    {
        node->data_ = find_min(node->right_)->data_;
    }
    else
    {
        SBinTree<obj>::node* tmp = node;
        node = (node->left_ != nullptr) ? node->left_ : node->right_;
        delete tmp;
    }

}

template <typename obj>
void SBinTree<obj>::remove(const obj& item)
{
    remove(root_,item);
}

template <typename obj>
typename SBinTree<obj>::node* SBinTree<obj>::find_min(SBinTree<obj>::node* node)const
{

    if(node == nullptr)//current node is null
    {
        return nullptr;
    }
    
    if(node->left_ == 0)//found furthest down  left node
    {
        return node;
    }

    return find_min(node->left_); //keep going left
}

template <typename obj>
const obj& SBinTree<obj>::find_min()const
{
    return find_min(root_)->data_; //return the min nodes data start with root node
}

template <typename obj>
typename SBinTree<obj>::node* SBinTree<obj>::find_max(SBinTree<obj>::node* node)const
{

    if(node == nullptr)//current node is null
    {
        return nullptr;
    }
    
    if(node->right_ == 0)//found furthest down right node
    {
        return node;
    }

    return find_max(node->right_); //keep going right
}

template <typename obj>
const obj& SBinTree<obj>::find_max()const
{
    return find_max(root_)->data_; //return the max nodes data start with root node
}

template <typename obj>
bool SBinTree<obj>::contains(SBinTree<obj>::node* node, const obj& item)const
{
    
    if(node == nullptr)
    {
        return false; //no match
    }
    else if(item < node->data_)
    {
        return contains(node->left_,item);//go down left tree
    }
    else if(item > node->data_)
    {
        return contains(node->right_,item);//go down right tree
    }
    else
    {
        return true;//matching case
    }
}

template <typename obj>
bool SBinTree<obj>::contains(const obj& item)const
{
   return contains(root_,item);
}

template <typename obj>
SBinTree<obj>& SBinTree<obj>::operator=(const SBinTree<obj>& rhs)
{
    
    root_ = clone(rhs.root_);
    return *this;

}
#endif //SBINTREE_H