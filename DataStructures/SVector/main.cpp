#include <iostream>
#include "SVector.h"

using namespace std;

int main()
{
    SVector<int> vec(2) ;

    vec.push(1);
    vec.push(2);
    printf("test contents %d\n", vec[0]);
    //should call double cap
    vec.push(3);
    vec.push(4);
    printf("test contents %d\n", vec[3]);



    return 0;
}
