#ifndef SDEQUE_H_INCLUDED
#define SDEQUE_H_INCLUDED

template <typename obj>
class SDeque
{
public:

    SDeque                  ()                {init();}
    ~SDeque                 ()                {}
    bool         empty      () const          {return size_ == 0;}
    unsigned int size       () const          {return size_;     }
    obj          front      ()                {return start_->data_;}
    obj          back       ()                {return end_->data_;  }
    obj          pop_front  ();
    obj          pop_back   ();
    void         push_front (const obj& item);
    void         push_back  (const obj& item);
    void         init       ();
    void         clear      ();

private:
    struct node
    {
        node(node* next = 0,node* prev = 0,const obj& data = obj {},char * name = ""):
            next_{next},prev_{prev},data_{data},name_{name}
            {
            }
    private:
        char* name_;
        node* next_;
        node* prev_;
        obj   data_;
        friend class SDeque<obj>;
    };

    node*        start_;
    node*        end_;
    unsigned int size_;


};

template <typename obj>
void SDeque<obj>::init()
{
    start_ = new node();
    end_ = new node();
    start_->name_ = "start";
    end_->name_ = "end";
    start_->next_ = end_;
    end_->prev_ = start_;

}

#endif // SDEQUE_H_INCLUDED
