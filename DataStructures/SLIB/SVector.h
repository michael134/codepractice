#ifndef SVECTOR_H_INCLUDED
#define SVECTOR_H_INCLUDED
/**********************************************************
|SVector.h                                                |
|By: Michael Carey                                        |
|Date: Feb 17 2020                                        |
|Purpose: Vector template class to store and index objects|
**********************************************************/

template <typename obj>

class SVector
{
public:
    enum {ALLOC = 10};

    SVector():
    size_{0},curr_alloc_{ALLOC}
    {
        data_ = new obj[ALLOC];
    }

    SVector(unsigned int alloc = ALLOC):
    size_{0},curr_alloc_{alloc}
    {
        data_ = new obj[alloc];
    }

    ~SVector(){ delete data_;}

    int  size     () const         {return size_;}
    bool empty    ()const          {return size_ == 0;}
    obj operator[](int index)      {return data_[index];}

    void push     (const obj& item)
    {
        if(size_ == curr_alloc_)
        {
            double_capacity();
            data_[size_] = item;
            ++size_;
        }
        else
        {
            data_[size_] = item;
            ++size_;
        }
    }

private:

    void double_capacity()
    {
        curr_alloc_ *=2;
        obj* tmp = new obj[curr_alloc_];
        for(unsigned int i = 0; i < size_; ++i)
        {
            tmp[i] = data_[i];
        }
        data_ = tmp;
        delete tmp;
    }

    obj * data_;
    unsigned int size_;
    unsigned int curr_alloc_;
};
#endif // SVECTOR_H_INCLUDED
