#include "SList.h"

using namespace std;

int main()
{
    printf("Constructing SList\n");
    SList<int> s_list;
    printf("Init object passed\n");

    printf("Testing Empty List\n");
    if(s_list.empty())
    {
        printf("Empty test passed\n");
    }
    printf("Adding Item\n");
    s_list.push_front(22,"twenty-two");
    printf("List Size: %d\n", s_list.size());
    printf("Adding Item\n");
    s_list.push_front(23,"twenty-three");
    printf("Adding Item\n");
    s_list.push_front(24,"twenty-four");
    printf("Adding Item\n");
    s_list.push_front(25,"twenty-five");
    printf("List Size: %d\n", s_list.size());
    int test = s_list.pop_front();
    printf("First item removed from list value : %d\n", test);
    test = s_list.pop_front();
    printf("Second item removed from list value : %d\n", test);
    test = s_list.pop_front();
    printf("Third item removed from list value : %d\n", test);
    test = s_list.pop_front();
    printf("Fourth item removed from list value : %d\n", test);
    printf("List Size: %d\n",s_list.size());
    test = s_list.pop_front();
    printf("empty item removed from list value : %d\n", test);
    test = s_list.pop_front();
    printf("empty item removed from list value : %d\n", test);
    s_list.push_front(25,"twenty-five");
    test = s_list.pop_front();
    printf("First item removed from list value : %d\n", test);
    test = s_list.pop_front();
    printf("empty item removed from list value : %d\n", test);
    test = s_list.pop_front();
    printf("empty item removed from list value : %d\n", test);
    printf("Adding Item\n");
    s_list.push_front(22,"twenty-two");
    printf("List Size: %d\n", s_list.size());
    printf("Adding Item\n");
    s_list.push_front(23,"twenty-three");
     printf("Adding Item\n");
    s_list.push_front(24,"twenty-four");
     printf("Adding Item\n");
    s_list.push_front(25,"twenty-five");
    printf("List Size: %d\n", s_list.size());
     printf("Adding Item\n");
    s_list.push_front(26,"twenty-six");
    printf("List Size: %d\n", s_list.size());
    printf("Adding Item\n");
    s_list.push_front(27,"twenty-seven");
     printf("Adding Item\n");
    s_list.push_front(28,"twenty-eight");
     printf("Adding Item\n");
    s_list.push_front(29,"twenty-nine");
    printf("List Size: %d\n", s_list.size());

    return 0;
}
