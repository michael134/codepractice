#ifndef SLIST_H_INCLUDED
#define SLIST_H_INCLUDED
#include <string>

/*
*********************************************
|SList.h                                    |
|By: Michael Carey                          |
|Date:Feb 14 2020                           |
|Purpose: Single Linked List Data Structure |
|Can also be used as a Stack Data Structure |
*********************************************
*/

template <typename obj>
class SList
{
public:

    SList  () {init();}      //call the reused init() function
    ~SList ();               //destructor
    SList(const SList & rhs);//copy constructor

    SList&      operator=  (const SList & rhs);
    void        push_front (const obj& item, std::string name = "");//add a new item to the front of the list
    obj         pop_front  ();                                      //remove and return front most item in the list
    void        init       ();                                      //initialize the list structure
    void        clear      ();                                      //clear the list
    const obj&  peek_front ()const{return head_->data_; }           //returns the first item in the list
    int         size       ()const{return size_;        }           //returns the current size of the list
    bool        empty      ()const{return size_ == 0;   }           //returns if the list is currently empty

private:

    struct node //structure to hold the data and pointer to next object in the list
    {
        //default constructor for node structure
        node(const obj & data = obj{}, node* next = nullptr,std::string name ="")
            :data_{data},next_{next},name_{name} {}
    private:
        obj data_;               //stored node data
        node* next_;             //pointer to next node
        std::string name_;       //string identifier for node
        friend class SList<obj>; //grant friend access to SList class
    };

    node* head_;                //head node item in the list points to first item in the list
    node* tail_;                //tail node item in the list next_ is always = to nullptr
    unsigned int size_;         //size of our list
};

template <typename obj>
SList<obj>::SList(const SList & rhs)
{
    init();                 //Initialize the list
    while( !rhs.empty() )   //while rhs isnt empty push its contents into the list
    {
        this.push_front(rhs.pop_front());
    }

}

template <typename obj>
SList<obj> & SList<obj>::operator=(const SList & rhs)
{
    size_ = rhs.size(); //copy rhs side into lhs
    head_ = rhs.head_;  //copy pointer to lhs head_
    tail_ = rhs.tail_;  //copy pointer to lhs tail_

    return *this;
}

template <typename obj>
void SList<obj>::push_front(const obj& item, std::string name) //add an item to the list
{

    if(empty())                                                //if the list is currently empty we need to set head to the temp node
    {
        node* tmp = new node(item, head_->next_,name);         //initialize node with head_->next_ to preserve structure
        head_ = tmp;                                           //make head point to the new item
    }
    else                                                       //if we are not adding the first item we need to set the head item(s) node to be its next_
    {
        node* tmp = new node(head_->data_, head_->next_,name); //build new node's data with head_->data_ and set tmp's next_ to head_->next_ to preserve the links
        head_->next_ = tmp;                                    //heads next_ will become the new node
        head_->data_ = item;                                   //insert the newest item at the front of the list
    }
    ++size_;                                                   //increment the list size
}

template <typename obj>
obj SList<obj>::pop_front()         //removes first item from the list and returns it
{
    obj retval ;                    //retval will be returned uninitialized if we use pop_front on an empty list

    if(!empty() && size() > 1)      //check for empty list
    {
        retval = head_->data_;      //set the return variable to the head node's data_ member
        node* tmp = head_;          //make a copy of head node
        head_ = head_->next_;       //set head to point to the next node in the list
        head_->name_ = tmp->name_;  //preserve the name of the head node
        delete tmp;                 //reclaim allocated memory
        --size_;                    //decrement the list's size
    }
    else if(size() == 1)            //if we are poping the last item we need to preserve relationship with the tail node
    {
        retval = head_->data_;      //set the return data to head_->data
        head_->next_ = tail_;       //make head_ point to tail because the list will be empty
        --size_;                    //decrement size
    }

    return retval;                  //return removed item if list empty returns nothing
}

template<typename obj>
void SList<obj>::init()
{
    head_ = new node();     //allocate new nodes for head and tail
    head_->name_ = "head";
    tail_ = new node();     //set their string identifiers
    tail_->name_ = "tail";
    head_->next_ = tail_;   //make head_-> next_ point to the tail_ node
    size_ = 0;              //set the initial size of the list to 0
}

template <typename obj>
void SList<obj>::clear()
{
    while(!empty())   //while we have items in the list
    {
        pop_front();  //remove item at front and reclaim its memory
    }
}

template <typename obj>
SList<obj>::~SList()
{
    clear();          //clear out the list
    delete tail_;     //delete tail node
    delete head_;     //delete head node
}
#endif // SLIST_H_INCLUDED
