#include "SDeque.h"

int main()
{
    SDeque<int> sdq;

    sdq.push_front(22 ,"22");
    sdq.push_front(23,"23");
    sdq.push_front(24,"24");


    int x = sdq.pop_front();
    int y = sdq.pop_front();
    int z = sdq.pop_front();


    return 0;
}
