#ifndef SDEQUE_H_INCLUDED
#define SDEQUE_H_INCLUDED


template <typename obj>
class SDeque
{
public:

    SDeque                  ()                {init();              }
    ~SDeque                 ()                {clear();             }
    bool         empty      () const          {return size_ == 0;   }
    unsigned int size       () const          {return size_;        }
    obj          front      ()                {return start_->data_;}
    obj          back       ()                {return end_->data_;  }
    obj          pop_front  ();
    obj          pop_back   ();
    void         push_front (const obj& item ,char* name = "");
    void         push_back  (const obj& item ,char* name);
    void         init       ();
    void         clear      ();

private:
    struct node
    {
        node(node* next = 0,node* prev = 0,const obj& data = obj {},char * name = ""):
            next_{next},prev_{prev},data_{data},name_{name}
            {
            }
    private:
        char* name_;
        node* next_;
        node* prev_;
        obj   data_;
        friend class SDeque<obj>;
    };

    node*        start_;
    node*        end_;
    unsigned int size_;
};

template<typename obj>
void SDeque<obj>::push_front(const obj& item ,char* name)
{
    node* tmp = new node(start_, 0 ,item,name);
    start_->prev_ = tmp;
    start_ = tmp;
    if(size_ == 0)
    {
        end_ = tmp;
    }
    ++size_;
}

template<typename obj>
obj SDeque<obj>::pop_front()
{
    obj retval;
    if(size_ > 0)
    {
        retval = start_->data_;
        node* tmp = start_;
        start_= start_->next_;
        delete tmp;
        --size_;
    }

    return retval;
}

template<typename obj>
void SDeque<obj>::clear()
{
    while(!empty())
    {
        pop_front();
    }
}

template <typename obj>
void SDeque<obj>::init()
{
    start_ = new node();
    end_ = new node();
    start_->name_ = "start";
    end_->name_ = "end";
    start_->next_ = end_;
    end_->prev_ = start_;
    size_=0;

}

#endif // SDEQUE_H_INCLUDED
