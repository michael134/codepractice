#ifndef APP_H
#define APP_H

#include <ExampleApplication.h>
#include <Ogre.h>
#include <string>
#include <vector>

class App: public ExampleApplication
{
    public:
        App();
        ~App();

        void set_skybox(std::string path);
        void add_light(std::string name);
        Ogre::Light* get_light(int index){return lights_[index];}
        std::vector<Ogre::Light*> * get_lights(){return &lights_;}

    protected:
        // Just override the mandatory create scene method
    void createScene(void);


    private:
    std::vector<Ogre::Light*> lights_;
};

#endif // APP_H
