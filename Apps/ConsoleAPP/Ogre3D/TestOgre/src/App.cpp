#include "App.h"
#include <string>

App::App()
{

}

App::~App()
{
    //dtor
}

void App::createScene()
{
    // Create the SkyBox
    set_skybox("Examples/CloudyNoonSkyBox");
}

void App::set_skybox(std::string path)
{
    mSceneMgr->setSkyBox(true, path);
}

void App::add_light(std::string name)
{
     Light* myLight = mSceneMgr->createLight(name);
     myLight->setType(Light::LT_POINT);
     lights_.push_back(myLight);
}
