//OGRE 3D Template                                                    |
//Michael Carey                                                       |
//Will Provide a Basic game engine template for an OGRE 3D Application|
//MARCH 18 2020                                                       |
//*********************************************************************

#include <App.h>
#include <iostream>
#include <cstdlib>

using namespace Ogre;

int main(int argc, char **argv)
{
    // Create application object
    App app;

    try{
        app.go();
    }
    catch( Exception& e ){

        std::cerr << "An exception has occured: " << e.getFullDescription();
    }



    // Create a light
    app.add_light("light0");
    app.add_light("light2");

    std::vector<Ogre::Light*> * lights = app.get_lights();

    (*lights)[0]->setPosition(0, 40, 0);
    (*lights)[0]->setDiffuseColour(1, 1, 1);
    (*lights)[0]->setSpecularColour(1, 1, 1);

    (*lights)[0]->setPosition(0, 20, 0);
    (*lights)[0]->setDiffuseColour(255,0,0);
    (*lights)[0]->setSpecularColour(1, 1, 1);




    return 0;
}
