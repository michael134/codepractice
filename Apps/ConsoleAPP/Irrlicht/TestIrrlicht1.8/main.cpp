#include <irrlicht.h>
#include<EventReceiver.h>
#include<iostream>
#include<vector>
#include<cstdlib>
#include<time.h>



/*
In the Irrlicht Engine, everything can be found in the namespace
'irr'. So if you want to use a class of the engine, you have to
write an irr:: before the name of the class. For example to use
the IrrlichtDevice write: irr::IrrlichtDevice. To get rid of the
irr:: in front of the name of every class, we tell the compiler
that we use that namespace from now on, and we will not have to
write that 'irr::'.
*/
using namespace irr;

/*
There are 5 sub namespaces in the Irrlicht Engine
*/
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

void spawn_tree(irr::scene::ISceneManager * smgr,irr::video::IVideoDriver* driver,core::vector3df pos,irr::scene::IMesh* mesh, core::stringw path,int height);

int main(int argc, char** argv)
{
    //event handler
    EventReceiver er;
    srand(time(NULL));

    IrrlichtDevice *device =
        createDevice(EDT_OPENGL,core::dimension2d<irr::u32>(800,600),16,false,false,false,&er );

    core::stringw path = "/home/mikey/Documents/codepractice/C++Projects/CodePractice/Apps/ConsoleAPP/Irrlicht/TestIrrlicht1.8/bin/Debug/";

    /*
    Get a pointer to the video driver, the SceneManager and the
    graphical user interface environment, so that
    we do not always have to write device->getVideoDriver(),
    device->getSceneManager() and device->getGUIEnvironment().
    */
    IVideoDriver* driver = device->getVideoDriver();
    ISceneManager* smgr = device->getSceneManager();
    IGUIEnvironment* guienv = device->getGUIEnvironment();

    core::stringw clouds = path+"clouds.jpg";
    smgr->addSkyDomeSceneNode(driver->getTexture(clouds.c_str()));
    smgr->setAmbientLight(video::SColor(255,245, 242, 66));

    core::vector3df sun_pos(25,30,25);
    smgr->addLightSceneNode(0,sun_pos,video::SColor(255,245, 242, 66),200);
    scene::IMeshSceneNode* sun = smgr->addSphereSceneNode();
    sun->setPosition(sun_pos);
    sun->setScale(core::vector3df(0.25f,0.25f,0.25f));

    core::stringw voxel = path+"voxel.obj";
    core::stringw grass = path+"grass.png";

    irr::scene::IMesh* mesh= smgr->getMesh(voxel.c_str());

    //build a voxel playground filled with nodes we can address
    std::vector<irr::scene::IMeshSceneNode*> nodes;
    std::vector<std::vector<irr::scene::IMeshSceneNode*>> grid;
    const int MAX_VOXELS = 50; //lets do a NxN grid
    const int NUM_TREES = 50;


    for(int row=0;row <MAX_VOXELS;++row) //rows of voxels
    {


        for(int clm = 0 ;clm<MAX_VOXELS;++clm) //clm of voxels
        {
           irr::scene::IMeshSceneNode* node = smgr->addMeshSceneNode(mesh); //build a node
           //node->setMaterialFlag(irr::video::EMF_LIGHTING,false);

           node->setMaterialTexture(0,driver->getTexture(grass.c_str()));
           nodes.push_back(node);//add new node to our vector
           //irr::scene::IMeshSceneNode::setPosition(core::vector3df(0,0,0));
           nodes[clm]->setPosition(core::vector3df(row,0,clm));//set to grid pos
           nodes[clm]->setScale(core::vector3df(0.5,0.5,0.5));//set to half scale
           nodes[clm]->setMaterialFlag(video::EMF_GOURAUD_SHADING ,true);
           nodes[clm]->setMaterialFlag(video::EMF_BACK_FACE_CULLING ,true);
           nodes[clm]->setMaterialFlag(video::EMF_FOG_ENABLE ,true);
           nodes[clm]->setMaterialFlag(video::EMF_USE_MIP_MAPS ,true);
        }
        grid.push_back(nodes);
        nodes.clear(); //clear each row
    }

    for(int i = 0; i < NUM_TREES; ++i)
    {
        int x = rand()% MAX_VOXELS;
        int z = rand()% MAX_VOXELS;
        int h = rand()%8 +5;

        spawn_tree(smgr,driver,core::vector3df(x,0,z),mesh,path,h);
        //spawn_tree(smgr,driver,core::vector3df(4,0,6),mesh,path,5);
        //spawn_tree(smgr,driver,core::vector3df(7,0,1),mesh,path,7);
    }

    //smgr->addCameraSceneNode(0, vector3df(0,30,-40), vector3df(0,5,0));
    smgr->addCameraSceneNodeFPS(0,60.0f,0.07f,-1,0,0,false,0.0f,false,true);

    //irr::scene::ISceneManager::addCameraSceneNodeFPS(0,60.0f,0.1f,-1,0,0,false,0.0f,false,true);

    /*
    Ok, now we have set up the scene, lets draw everything:
    We run the device in a while() loop, until the device does not
    want to run any more. This would be when the user closed the window
    */
    while(device->run())
    {
        core::stringw str = L"Testing Voxels Irrlicht FPS:";
        int fps = driver->getFPS();
        str += fps;
        device->setWindowCaption(str.c_str());

        if(er.IsKeyDown(irr::KEY_ESCAPE)) //esc button to exit
            {
               std::cout << "ESC Pressed\n";
               return 0;
            }

        /*
        Anything can be drawn between a beginScene() and an endScene()
        call. The beginScene clears the screen with a color and also the
        depth buffer if wanted. Then we let the Scene Manager and the
        GUI Environment draw their content. With the endScene() call
        everything is presented on the screen.
        */
        driver->beginScene(true, true, SColor(0,200,200,200));

        smgr->drawAll();
        guienv->drawAll();

        driver->endScene();
    }

    /*
    After we are finished, we have to delete the Irrlicht Device
    created before with createDevice(). In the Irrlicht Engine,
    you will have to delete all objects you created with a method or
    function which starts with 'create'. The object is simply deleted
    by calling ->drop().

    */
    device->drop();

    return 0;
}



void spawn_tree(irr::scene::ISceneManager * smgr,irr::video::IVideoDriver* driver,core::vector3df pos,irr::scene::IMesh* mesh, core::stringw path,int height)
{
    core::stringw leaves_tex = path+"leaves.png";
    core::stringw wood_tex = path+"wood.png";
    int levels = 3;

    //build out the trunk of the tree
    for(int h = 1;h< height;++h)
    {
          //wood node
        irr::scene::IMeshSceneNode* wood = smgr->addMeshSceneNode(mesh); //build a node
        //wood->setMaterialFlag(irr::video::EMF_LIGHTING,false);
        wood->setMaterialTexture(0,driver->getTexture(wood_tex.c_str()));
        wood->setPosition(core::vector3df(pos.X,h,pos.Z));//set to grid pos
        wood->setScale(core::vector3df(0.5,0.5,0.5));//set to half scale
        wood->setMaterialFlag(video::EMF_GOURAUD_SHADING ,true);
        wood->setMaterialFlag(video::EMF_BACK_FACE_CULLING ,true);
        wood->setMaterialFlag(video::EMF_FOG_ENABLE ,true);
        wood->setMaterialFlag(video::EMF_USE_MIP_MAPS ,true);
    }

    //build the top most leaf node
    //pos is above the top most trunk node
    irr::scene::IMeshSceneNode* leaf = smgr->addMeshSceneNode(mesh); //build a node
    //leaf->setMaterialFlag(irr::video::EMF_LIGHTING,false);
    leaf->setMaterialTexture(0,driver->getTexture(leaves_tex.c_str()));
    leaf->setPosition(core::vector3df(pos.X,height,pos.Z));//set to grid pos
    leaf->setScale(core::vector3df(0.5,0.5,0.5));//set to half scale
    leaf->setMaterialFlag(video::EMF_GOURAUD_SHADING ,true);
    leaf->setMaterialFlag(video::EMF_BACK_FACE_CULLING ,true);
    leaf->setMaterialFlag(video::EMF_FOG_ENABLE ,true);
    leaf->setMaterialFlag(video::EMF_USE_MIP_MAPS ,true);
    leaf->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL);
    //add multiple layers of leaves
    //start at level
    //subtract level from height to go down a few nodes
    //build up from there
    for(int l = levels;l > 0 ;--l)
    {
        //using a reference node we go in each direction and add a leaf node
        //Leaf in the -X direction
        irr::scene::IMeshSceneNode* leaf1 = smgr->addMeshSceneNode(mesh); //build a node
        //leaf1->setMaterialFlag(irr::video::EMF_LIGHTING,false);
        leaf1->setMaterialTexture(0,driver->getTexture(leaves_tex.c_str()));
        leaf1->setPosition(core::vector3df(pos.X-1,height-l,pos.Z));//set to grid pos
        leaf1->setScale(core::vector3df(0.5,0.5,0.5));//set to half scale
        leaf1->setMaterialFlag(video::EMF_GOURAUD_SHADING ,true);
        leaf1->setMaterialFlag(video::EMF_BACK_FACE_CULLING ,true);
        leaf1->setMaterialFlag(video::EMF_FOG_ENABLE ,true);
        leaf1->setMaterialFlag(video::EMF_USE_MIP_MAPS ,true);
        leaf1->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL);
        //Leaf in the +X direction
        irr::scene::IMeshSceneNode* leaf2 = smgr->addMeshSceneNode(mesh); //build a node
        //leaf2->setMaterialFlag(irr::video::EMF_LIGHTING,false);
        leaf2->setMaterialTexture(0,driver->getTexture(leaves_tex.c_str()));
        leaf2->setPosition(core::vector3df(pos.X+1,height-l,pos.Z));//set to grid pos
        leaf2->setScale(core::vector3df(0.5,0.5,0.5));//set to half scale
        leaf2->setMaterialFlag(video::EMF_GOURAUD_SHADING ,true);
        leaf2->setMaterialFlag(video::EMF_BACK_FACE_CULLING ,true);
        leaf2->setMaterialFlag(video::EMF_FOG_ENABLE ,true);
        leaf2->setMaterialFlag(video::EMF_USE_MIP_MAPS ,true);
        leaf2->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL);
        //Leaf in the -Y direction
        irr::scene::IMeshSceneNode* leaf3 = smgr->addMeshSceneNode(mesh); //build a node
        //leaf3->setMaterialFlag(irr::video::EMF_LIGHTING,false);
        leaf3->setMaterialTexture(0,driver->getTexture(leaves_tex.c_str()));
        leaf3->setPosition(core::vector3df(pos.X,height-l,pos.Z-1));//set to grid pos
        leaf3->setScale(core::vector3df(0.5,0.5,0.5));//set to half scale
        leaf3->setMaterialFlag(video::EMF_GOURAUD_SHADING ,true);
        leaf3->setMaterialFlag(video::EMF_BACK_FACE_CULLING ,true);
        leaf3->setMaterialFlag(video::EMF_FOG_ENABLE ,true);
        leaf3->setMaterialFlag(video::EMF_USE_MIP_MAPS ,true);
        leaf3->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL);
        //Leaf in the +Y direction
        irr::scene::IMeshSceneNode* leaf4 = smgr->addMeshSceneNode(mesh); //build a node
        //leaf4->setMaterialFlag(irr::video::EMF_LIGHTING,false);
        leaf4->setMaterialTexture(0,driver->getTexture(leaves_tex.c_str()));
        leaf4->setPosition(core::vector3df(pos.X,height-l,pos.Z+1));//set to grid pos
        leaf4->setScale(core::vector3df(0.5,0.5,0.5));//set to half scale
        leaf4->setMaterialFlag(video::EMF_GOURAUD_SHADING ,true);
        leaf4->setMaterialFlag(video::EMF_BACK_FACE_CULLING ,true);
        leaf4->setMaterialFlag(video::EMF_FOG_ENABLE ,true);
        leaf4->setMaterialFlag(video::EMF_USE_MIP_MAPS ,true);
        leaf4->setMaterialType(video::EMT_TRANSPARENT_ALPHA_CHANNEL);


    }



}


