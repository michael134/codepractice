#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "PetopiaInterface.h"

#include<string>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void                     on_ScheduleWidget_cellChanged       (int row, int column);
    void                     on_CalculateHoursButton_clicked     ();
    void                     on_RemoveEButton_clicked            ();
    void                     on_addEButton_clicked               ();
    void                     on_quitButton_clicked               ();
    void                     on_SaveButton_clicked               ();
    void                     on_refreshButton_clicked            ();
    void                     on_actionQuit_triggered             ();
    void                     on_actionAbout_triggered            ();
    void                     on_actionRefresh_triggered          ();
    void                     on_actionRemove_Employee_2_triggered();
    void                     on_actionAdd_Employee_2_triggered   ();
    void                     on_actionPrint_triggered            ();
    void                     on_PreviewButton_clicked            ();
    void                     on_TableButton_clicked              ();
    void                     on_actionLoad_Preview_triggered     ();
    void                     on_ScheduleWidget_cellClicked       (int row, int column);
    void                     on_PasswordpushButton_clicked       ();

    void on_PassChangepushButton_clicked();

    void on_actionChange_Password_triggered();

    void on_actionLoad_File_triggered();

    void on_actionLoad_Default_triggered();

    void on_actionSave_triggered();

    void on_actionNew_File_triggered();

private:
    void                     set_status                          (std::string new_status);
    void                     load_employees                      (bool reused);
    QString                  convert_string_to_table_item        (std::string item);
    void                     add_employee                        ();
    void                     reload_employees                    ();
    void                     save_schedule                       (std::string file_path);
    float                    convert_time_to_float               (std::string time_str);
    std::vector<std::string> split_string                        (std::string str, char delimiter);
    void                     eraseSubStr                         (std::string & mainStr, const std::string & toErase);
    void                     show_pass_dialog                    ();
    void                     hide_pass_dialog                    ();
    void                     show_ui                             ();
    void                     hide_ui                             ();
    std::string              encryptDecrypt                      (std::string toEncrypt);
    void                     save_configuration                  ();

    Ui::MainWindow *ui;
    PetopiaInterface pi_;
    std::string curr_employee_selected;
    int curr_row_selected;
    const QString VERSION_NUMBER = "v1.0.6";
    std::string current_file_path;

};
#endif // MAINWINDOW_H
