#ifndef PETOPIAINTERFACE_H
#define PETOPIAINTERFACE_H
#include "Petopia.h"
#include <string>

/***************************************\
|PETOPIAINTERFACE_H                     |
|By:Michael Carey                       |
|Date: Feb/19/2020                      |
|Purpose:class to interact with business|
|employees and schedules                |
\***************************************/

class PetopiaInterface
{
    public:
        PetopiaInterface();
        PetopiaInterface(std::string cmd);
        ~PetopiaInterface();

        void                      open_config    ();                      //open the configuration file that stores process data
        void                      open_file      (std::string file_path); //open employee schedule file
        void                      parse_employee (const std::vector<std::string>& employee);
        std::vector<std::string>  parse_line     (std::string str);
        std::string               get_status     (){return status_;} //return the current status

        Petopia petopia_; //store refrence to petopia business class
        std::string cmd_; //current command
        std::string status_;//current status
        std::string current_path_;
};

#endif // PETOPIAINTERFACE_H
