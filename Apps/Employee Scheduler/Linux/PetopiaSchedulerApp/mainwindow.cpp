﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "PetopiaInterface.h"
#include <QApplication>
#include <QPrinter>
#include <QPrintDialog>
#include <QPrinterInfo>
#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->OutputEdit->setHidden(true);
    current_file_path = pi_.current_path_;
    if(pi_.petopia_.SECURITY_ENABLED)
    {
        hide_ui();
        ui->PassChangepushButton->hide();
        ui->NewPasswdlabel->hide();
        show_pass_dialog();
    }
    else
    {
        load_employees(false);
        hide_pass_dialog();

    }


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_RemoveEButton_clicked()
{
    pi_.petopia_.remove_employee(curr_row_selected); //delete the current employee from the table
    load_employees(true);
    set_status("Employee Removed");
}

void MainWindow::on_addEButton_clicked()
{
    add_employee();
    set_status("New Employee Added");
}

void MainWindow::on_quitButton_clicked()
{
    exit(0);
}

void MainWindow::save_schedule(std::string file_path = "Default.txt") //rebuild the saved configuration file
{
    std::ofstream ofs;
    ofs.open(file_path, std::ofstream::out | std::ofstream::trunc);

    //write standard txt back to file
    ofs << "#*****************************************************************************************************************\n"
    <<"#Employee File\n"
    <<"#Help Section\n"
    <<"#Employee Info inserts an employee into the schedule\n"
    <<"#format Name,Hours,Monday,Monday_Hours,Tuesday,Tuesday_Hours,Wednesday,Wednesday_Hours,Thursday,Thursday_Hours\n"
    <<"#,Friday,Friday_Hours,Saturday, Saturday_Hours,Sunday,Sunday_Hours\n"
    <<"#Days of the week have the following codes\n"
    <<"#Monday=0 Tuesday = 1 Wednesday = 2 Thursday = 3 Friday = 4 Saturday = 5 Sunday = 6\n"
    <<"#*****************************************************************************************************************\n";
    //<<"#PWD\n"
    //<<pi_.petopia_.password_
    //<<"\n";

    for(unsigned int i = 0;i<pi_.petopia_.employees_.size();++i) //for each employee write the current data back to the save file
    {

        ofs << "#Employee Info\n";
        ofs << pi_.petopia_.employees_[i].get_name()
               <<","<< pi_.petopia_.employees_[i].hours_
               << "," << 0 << "," << pi_.petopia_.employees_[i].get_schedule().get_day_schedule(0).time_
               << "," << 1 << "," << pi_.petopia_.employees_[i].get_schedule().get_day_schedule(1).time_
               << "," << 2 << "," << pi_.petopia_.employees_[i].get_schedule().get_day_schedule(2).time_
               << "," << 3 << "," << pi_.petopia_.employees_[i].get_schedule().get_day_schedule(3).time_
               << "," << 4 << "," << pi_.petopia_.employees_[i].get_schedule().get_day_schedule(4).time_
               << "," << 5 << "," << pi_.petopia_.employees_[i].get_schedule().get_day_schedule(5).time_
               << "," << 6 << "," << pi_.petopia_.employees_[i].get_schedule().get_day_schedule(6).time_ << "\n";
    }

    ofs.close(); //close our configuration file
}

void MainWindow::save_configuration()
{

    std::ofstream ofs;
    ofs.open("Config.txt", std::ofstream::out | std::ofstream::trunc);

    //write standard txt back to file
    ofs << "#*****************************************************************************************************************\n"
    <<"#Configuration File\n"
    <<"#Stores Program Information\n"
    <<"#Auto Generated On Save\n"
    <<"#*****************************************************************************************************************\n"
    <<"#PWD\n"
    <<pi_.petopia_.password_
    <<"\n"
    <<"#PATH\n"
    <<current_file_path
    <<"\n";
}


void MainWindow::on_SaveButton_clicked()
{
  save_schedule(current_file_path);
  save_configuration();
  set_status("Saved to " + current_file_path);
}

void MainWindow::set_status(std::string new_status)
{
    QString status(new_status.c_str());
    ui->statusLabel->setText(status);
}

void MainWindow::load_employees(bool reused)
{
    ui->ScheduleWidget->clearContents(); //clean up table for reuse of this function

    if(!reused)
    {
        for(unsigned int i = 0;i< pi_.petopia_.employees_.size(); ++i) //for all the loaded employees
        {
            ui->ScheduleWidget->insertRow(i);
           // QWidget* q = ui->ScheduleWidget->cellWidget(0,0);
           //insert into table all the attributes
           ui->ScheduleWidget->setItem(i,0, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].name_))); //add name
           ui->ScheduleWidget->setItem(i,1, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[0].time_)));//monday schedule
           ui->ScheduleWidget->setItem(i,2, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[1].time_)));//tuesday schedule
           ui->ScheduleWidget->setItem(i,3, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[2].time_)));//wednesday schedule
           ui->ScheduleWidget->setItem(i,4, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[3].time_)));//thursday schedule
           ui->ScheduleWidget->setItem(i,5, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[4].time_)));//friday schedule
           ui->ScheduleWidget->setItem(i,6, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[5].time_)));//saturday schedule
           ui->ScheduleWidget->setItem(i,7, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[6].time_)));//sunday schedule
           ui->ScheduleWidget->setItem(i,8, new QTableWidgetItem(convert_string_to_table_item(std::to_string(pi_.petopia_.employees_[i].hours_))));//add employee hours
        }
    }
    else if(reused)
    {
        for(unsigned int i = 0;i< pi_.petopia_.employees_.size(); ++i) //for all the loaded employees
        {
           // QWidget* q = ui->ScheduleWidget->cellWidget(0,0);
           //insert into table all the attributes
           ui->ScheduleWidget->setItem(i,0, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].name_))); //add name
           ui->ScheduleWidget->setItem(i,1, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[0].time_)));//monday schedule
           ui->ScheduleWidget->setItem(i,2, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[1].time_)));//tuesday schedule
           ui->ScheduleWidget->setItem(i,3, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[2].time_)));//wednesday schedule
           ui->ScheduleWidget->setItem(i,4, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[3].time_)));//thursday schedule
           ui->ScheduleWidget->setItem(i,5, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[4].time_)));//friday schedule
           ui->ScheduleWidget->setItem(i,6, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[5].time_)));//saturday schedule
           ui->ScheduleWidget->setItem(i,7, new QTableWidgetItem(convert_string_to_table_item(pi_.petopia_.employees_[i].get_schedule().days_[6].time_)));//sunday schedule
           ui->ScheduleWidget->setItem(i,8, new QTableWidgetItem(convert_string_to_table_item(std::to_string(pi_.petopia_.employees_[i].hours_))));//add employee hours
        }

    }

    set_status("Employees Loaded Successfully From " + current_file_path); //set our status after loading all the employee fields
}

QString  MainWindow::convert_string_to_table_item(std::string item)
{

    QString new_item(item.c_str());
    return new_item;
}

void MainWindow::on_ScheduleWidget_cellClicked(int row, int column)
{
    if(column == 0)
    {
        ui->RemoveEButton->setEnabled(true);
        curr_employee_selected = ui->ScheduleWidget->item(row,column)->text().toStdString();
        curr_row_selected = row;
    }
    else
    {
        ui->RemoveEButton->setEnabled(false);
        curr_employee_selected = "";
        curr_row_selected = -1;
    }

}
void MainWindow::add_employee()
{
    ui->ScheduleWidget->insertRow(pi_.petopia_.employees_.size());
    pi_.petopia_.add_employee("New Employee", 40);
    load_employees(true);
}

void MainWindow::reload_employees()
{
    pi_.petopia_.employees_.clear(); //clean out vector of employees
    //pi_.open_config(); //
    pi_.open_file(current_file_path);
    load_employees(true);
}

void MainWindow::on_refreshButton_clicked()
{
    reload_employees();
}

void MainWindow::on_actionQuit_triggered()
{
    exit(0);
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("Employee Scheduler " + VERSION_NUMBER + "\nProgrammed By Michael Carey Feb 2020");
    msgBox.exec();
}

void MainWindow::on_actionRefresh_triggered()
{
    reload_employees();
}

void MainWindow::on_actionRemove_Employee_2_triggered()
{
    pi_.petopia_.remove_employee(curr_row_selected); //delete the current employee from the table
    load_employees(true);
    set_status("Employee Removed");
}

void MainWindow::on_actionAdd_Employee_2_triggered()
{
    add_employee();
    set_status("New Employee Added");
}

void MainWindow::on_actionPrint_triggered()
{
   QString p_name = "Test";
   QPrinter printer;
   //printer.setPrinterSelectionOption(p_name);
   printer.setPrinterName(p_name);
   QPrintDialog dialog(&printer,this);
   if(dialog.exec() == QDialog::Rejected)
   {
       set_status("Print Job Failed");
   }
   else
   {
       ui->OutputEdit->print(&printer);
   }
}

void MainWindow::on_PreviewButton_clicked()
{
    ui->OutputEdit->setHidden(false);
    ui->ScheduleWidget->setHidden(true);
}

void MainWindow::on_TableButton_clicked()
{
    ui->OutputEdit->setHidden(true);
    ui->ScheduleWidget->setHidden(false);
}

void MainWindow::on_actionLoad_Preview_triggered()
{
    ui->OutputEdit->setText("");
    pi_.petopia_.curr_schedule_print_out_ =""; //clear out previous previews

    pi_.petopia_.print_schedule();
    set_status("Building Schedule Preview");
    ui->OutputEdit->setText(convert_string_to_table_item(pi_.petopia_.curr_schedule_print_out_));
    //pi_.petopia_.curr_schedule_print_out_;
    set_status("Schedule Preview Built");
}

void MainWindow::on_ScheduleWidget_cellChanged(int row, int column) //whenever a cell in our table is changed update the data in memory
{
    QString data = ui->ScheduleWidget->model()->index(row,column).data().toString();

   switch(column)
   {
   //name
   case 0:
        pi_.petopia_.employees_[row].set_name(data.toStdString());
       break;
   //monday
   case 1:
       pi_.petopia_.employees_[row].schedule_.days_[0].time_ = data.toStdString();
       break;

   case 2:
       pi_.petopia_.employees_[row].schedule_.days_[1].time_ = data.toStdString();
       break;

   case 3:
       pi_.petopia_.employees_[row].schedule_.days_[2].time_ = data.toStdString();
       break;

   case 4:
       pi_.petopia_.employees_[row].schedule_.days_[3].time_ = data.toStdString();
       break;

   case 5:
       pi_.petopia_.employees_[row].schedule_.days_[4].time_ = data.toStdString();
       break;

   case 6:
       pi_.petopia_.employees_[row].schedule_.days_[5].time_ = data.toStdString();
       break;

   case 7:
       pi_.petopia_.employees_[row].schedule_.days_[6].time_ = data.toStdString();
       break;
   //hours
   case 8:
        pi_.petopia_.employees_[row].set_hours(data.toFloat());
       break;

   default:
       break;
   }
}

void MainWindow::on_CalculateHoursButton_clicked()
{
    for(unsigned int row = 0; row < pi_.petopia_.employees_.size(); ++row)
    {
        float calc_hours = 0;

        for(unsigned int clm = 1; clm < 7; clm++)//add togeather all the columns of hours
        {
            QString data = ui->ScheduleWidget->model()->index(row,clm).data().toString();
            std::string str = data.toStdString();
            calc_hours += convert_time_to_float(str);
        }
        pi_.petopia_.employees_[row].hours_ = calc_hours;

    }
    save_schedule(current_file_path);
    set_status("Saving Schedule");
    load_employees(true);
    set_status("Hours Calculated");
}

float MainWindow::convert_time_to_float(std::string time_str)
{
    bool first_am = false, second_am = false;
    float retval = 0;
    if(time_str != "OFF" && time_str != "Any" && time_str != "off" && time_str != "Off"
            && time_str != "ANY" && time_str != "any") // if we are not working(OFF day) dont calculate
    {
        //split the input string 8:00AM-5:00PM into 2 strings and store in vector
        //vec[0]= 8:00AM
        //vec[1]= 5:00PM
        std::vector<std::string> result = split_string(time_str,'-');

        //calculate the timespan AM-PM AM-AM PM-PM store isnt open for PM-AM shift
        if(  result[0].find("AM") != std::string::npos)
        {
            //found AM
            first_am = true;
            //remove am from string
           eraseSubStr( result[0],"AM");//remove time identifier
        }
        else if(result[0].find("PM") != std::string::npos)
        {
            //found PM
            //remove PM from string
             eraseSubStr( result[0],"PM");//remove time identifier
        }
        if(  result[1].find("AM") != std::string::npos)
        {
            //found AM
            second_am = true;
            eraseSubStr( result[1],"AM");//remove time identifier
        }
        else if(result[1].find("PM") != std::string::npos)
        {
            //found PM
            eraseSubStr( result[1],"PM");//remove time identifier
        }

        std::vector<std::string> v1,v2;
        //build out 2 vectors of the time broken down into
        //v[0] hour string
        //v[1] minute string
        v1 = split_string(result[0],':');
        v2 = split_string(result[1],':');
        int hour_diff=0, minute_diff=0;

        if(first_am && !second_am) // AM - PM shift
        {
            hour_diff = std::stoi(v2[0]) + 12;//convert to 24h
            hour_diff -= std::stoi(v1[0]);
        }
        else if(first_am && second_am) //AM -AM shift
        {
            hour_diff = (std::stoi(v2[0]) - std::stoi(v1[0]));
        }

        else if(!first_am && !second_am) //PM-PM shift
        {
            hour_diff = (std::stoi(v2[0]) - std::stoi(v1[0]));
        }

        //calculate the minute difference
        if(std::stoi(v2[1]) >= std::stoi(v1[1]))
        {
            minute_diff = std::stoi(v2[1]) - std::stoi(v1[1]);
        }
        else if(std::stoi(v2[1]) < std::stoi(v1[1]))
        {
            minute_diff =  std::stoi(v1[1]) - std::stoi(v2[1]);
        }
        else
        {
            minute_diff = 0;
        }

        hour_diff *= 60;         //convert hours to minutes
        minute_diff+= hour_diff;  //total number of minutes
        retval = minute_diff / 60.00; //return the total minutes divided by 60 minutes to return hours difference
    }
    return retval;
}

std::vector<std::string> MainWindow::split_string(std::string str, char delimiter)
{
    std::vector<std::string> result;
    std::stringstream s_stream(str); //create string stream from the string
    while(s_stream.good())
    {
       std::string substr;
       getline(s_stream, substr, delimiter); //get first string delimited by dash
       result.push_back(substr);
    }
    return result;
}

void MainWindow::eraseSubStr(std::string & mainStr, const std::string & toErase)
{
    // Search for the substring in string
    size_t pos = mainStr.find(toErase);

    if (pos != std::string::npos)
    {
        // If found then erase it from string
        mainStr.erase(pos, toErase.length());
    }
}

void MainWindow::on_PasswordpushButton_clicked()
{
    if(ui->PasswordlineEdit->text().toStdString() == encryptDecrypt( pi_.petopia_.password_ ))
    {
        set_status(pi_.get_status());                               //set the status after loading the config get it from the petopia Interface
        load_employees(false);                                      //load the employees stored in memory
        show_ui();
        hide_pass_dialog();
    }
    else
    {
        set_status("Wrong Password Entered");
    }
}

void MainWindow::show_pass_dialog()
{
    ui->PasswordlineEdit->setEchoMode(QLineEdit::Password);
    ui->Passwordlabel->show();
    ui->PasswordlineEdit->show();
    ui->PasswordpushButton->show();

}
void MainWindow:: hide_pass_dialog()
{
    ui->Passwordlabel->hide();
    ui->PasswordlineEdit->hide();
    ui->PasswordpushButton->hide();
}
void MainWindow::show_ui()
{

    ui->actionAdd_Employee_2->blockSignals(false);
    ui->actionLoad_Preview->blockSignals(false);
    ui->actionPrint->blockSignals(false);
    ui->actionRefresh->blockSignals(false);
    ui->actionRemove_Employee_2->blockSignals(false);
    ui->actionSave->blockSignals(false);
    ui->addEButton->show();
    ui->CalculateHoursButton->show();
   // ui->centralWidget->show();
    ui->OutputEdit->show();
    ui->PreviewButton->show();
    ui->refreshButton->show();
    ui->RemoveEButton->show();
    ui->SaveButton->show();
    ui->ScheduleWidget->show();
    ui->TableButton->show();
}
void MainWindow::hide_ui()
{

    ui->actionAdd_Employee_2->blockSignals(true);
    ui->actionLoad_Preview->blockSignals(true);
    ui->actionPrint->blockSignals(true);
    ui->actionRefresh->blockSignals(true);
    ui->actionRemove_Employee_2->blockSignals(true);
    ui->actionSave->blockSignals(true);
    ui->addEButton->hide();
    ui->CalculateHoursButton->hide();
   // ui->centralWidget->hide();
    ui->OutputEdit->hide();
    ui->PreviewButton->hide();
    ui->refreshButton->hide();
    ui->RemoveEButton->hide();
    ui->SaveButton->hide();
    ui->ScheduleWidget->hide();
    ui->TableButton->hide();
}

void MainWindow::on_PassChangepushButton_clicked()
{
    pi_.petopia_.password_ =  encryptDecrypt(  ui->PasswordlineEdit->text().toStdString() );
    hide_pass_dialog();
    show_ui();
    ui->PassChangepushButton->hide();
    ui->NewPasswdlabel->hide();
    set_status("Password Changed Please Save Changes");
}

void MainWindow::on_actionChange_Password_triggered()
{

    hide_ui();
    show_pass_dialog();
    ui->Passwordlabel->hide();
    ui->PassChangepushButton->show();
    ui->NewPasswdlabel->show();
    ui->PasswordlineEdit->setText("");
    ui->PasswordlineEdit->setEchoMode(QLineEdit::Normal);
}

std::string MainWindow::encryptDecrypt(std::string toEncrypt) {
    char key = 'K'; //Any char will work
    std::string output = toEncrypt;

    for (unsigned int i = 0; i < toEncrypt.size(); i++)
        output[i] = toEncrypt[i] ^ key;

    return output;
}

void MainWindow::on_actionLoad_File_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Open File","../Documents/");
    current_file_path = file_name.toStdString();
    pi_.open_file(current_file_path);
    //load_employees(true);
    save_configuration();
    set_status(current_file_path + " Loaded Successfully");
    reload_employees();
}

void MainWindow::on_actionLoad_Default_triggered()
{
    pi_.open_file("Default.txt");
    current_file_path = "Default.txt";
    pi_.current_path_ = "Default.txt";
    save_schedule("Default.txt");
    save_configuration();
    //load_employees(true);
    set_status(current_file_path + " Loaded Successfully");
    reload_employees();


}

void MainWindow::on_actionSave_triggered()
{
    save_schedule(current_file_path);
    save_configuration();
    set_status("Saved to " + current_file_path);
    reload_employees();
}

void MainWindow::on_actionNew_File_triggered()
{
    pi_.open_file("Default.txt"); //load default values into memory
    QString file_name = QFileDialog::getSaveFileName(this,"Create New File","../Documents/");
    current_file_path = file_name.toStdString();
    //if a user does not put .txt at the end of the filename
    if(current_file_path[current_file_path.size()-1]!= 't' && current_file_path[current_file_path.size()-2]!= 'x'&& current_file_path[current_file_path.size()-3]!= 't'&& current_file_path[current_file_path.size()-4]!= '.')
    {
        current_file_path +=".txt";
    }
    //pi_.open_file(current_file_path);
    //load_employees(true);
    save_schedule(current_file_path);
    save_configuration();
    set_status(current_file_path + " Created Successfully");
    reload_employees();

}
