#ifndef PETOPIA_H
#define PETOPIA_H
#include "PEmployee.h"
#include <string>
#include <vector>

/***************************************\
|PETOPIA_H                              |
|By:Michael Carey                       |
|Date: Feb/19/2020                      |
|Purpose:class to represent Business    |
|holds employees                        |
\***************************************/

class Petopia
{
    public:
        Petopia();
        ~Petopia();

        void init                     ();
        int  size                     (){return size_;}
        void set_employee_day_schedule(std::string name, std::string day , std::string schedule);
        void add_employee             (std::string name, int hours);
        void remove_employee          (int index){employees_.erase(employees_.begin() + index); --size_;}
        int  day_to_int               (std::string day);
        int  find_employee            (std::string name);
        void print_schedule();

        //public data members so that the interface can access all the way down to day
        int num_employees_;
        int size_;                            //current amount of employees
        std::vector<PEmployee> employees_;    //stored employees
        std::string curr_schedule_print_out_; //current built string for a schedule preview
        std::string password_;
        const bool SECURITY_ENABLED = 1;

};

#endif // PETOPIA_H
