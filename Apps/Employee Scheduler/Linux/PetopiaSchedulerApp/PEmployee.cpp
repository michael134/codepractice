#include "PEmployee.h"
#include <string>
#include <iostream>

/***************************************\
|PEMPLOYEE_CPP                          |
|By:Michael Carey                       |
|Date: Feb/19/2020                      |
|Purpose:class to hold Petopia employees|
|and manage their respective schedules  |
\***************************************/

using namespace std;


PEmployee::PEmployee()
{
    //ctor
}

PEmployee::~PEmployee()
{
    //dtor
}

std::string PEmployee::print_day_schedule(int day) //return the schedule string for a given day of the week {MONDAY = 0,TUESDAY = 0,WEDNESDAY = 0,THURSDAY = 0,FRIDAY = 0,SATURDAY = 0,SUNDAY = 0}
{
    std::string ret = schedule_.days_[day].time_;
    return ret;
}

std::string PEmployee::print_schedule()
{
    string ret = "";

    cout  << "SCHEDULE:" << get_name() <<endl;
    ret += get_name() +" HOURS: " + std::to_string(get_hours()) +"\n";
    cout<<"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
    ret += "-------------------------------------------------------------------------------------------------------------------------------------------------\n";

    cout<<"Mon: " << print_day_schedule(schedule_.Monday) <<
          "Tues: " << print_day_schedule(schedule_.Tuesday)<<
          "Wed: "<< print_day_schedule(schedule_.Wednesday)<<
          "Thur: "<< print_day_schedule(schedule_.Thursday)<<
          "Fri:"<< print_day_schedule(schedule_.Friday)<<
          "Sat: "<< print_day_schedule(schedule_.Saturday)<<
          "Sun: "<< print_day_schedule(schedule_.Sunday)<< "\n";

    ret +="Mon: " + print_day_schedule(schedule_.Monday)+
          " Tues: " + print_day_schedule(schedule_.Tuesday)+
          " Wed: "+ print_day_schedule(schedule_.Wednesday)+
          " Thurs: "+ print_day_schedule(schedule_.Thursday)+
          " Fri: "+ print_day_schedule(schedule_.Friday)+
          " Sat: "+ print_day_schedule(schedule_.Saturday)+
          " Sun: "+ print_day_schedule(schedule_.Sunday)+ "\n";

    cout<<"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
    ret += "-------------------------------------------------------------------------------------------------------------------------------------------------\n";

    return ret;
}



