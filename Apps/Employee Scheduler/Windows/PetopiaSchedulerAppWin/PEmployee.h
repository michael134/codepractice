#ifndef PEMPLOYEE_H
#define PEMPLOYEE_H

/***************************************\
|PEMPLOYEE_H                            |
|By:Michael Carey                       |
|Date: Feb/19/2020                      |
|Purpose:class to hold Petopia employees|
|and manage their respective schedules  |
\***************************************/

#include<string>

class PEmployee
{
    public:
        struct Schedule //holds the days of the week worked for an employee
        {
            public:
                enum Days_Of_Week{Monday=0,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday}; //all the days of the week work as indexes into the days_ array

                struct Day //work day structure
                {
                    Day(std::string time = "OFF"):
                        time_(time)
                        {}
                    Day& operator=(const Day& rhs){day_ = rhs.day_; time_ = rhs.time_; return *this;}
                    int day_;
                    std::string time_;

                    void set_time(std::string new_time)
                    {
                        int diff = 13 - new_time.size();
                        std::string buff(diff, ' ');
                        time_=new_time + buff;
                    }
                };

                Schedule()
                {
                    days_ = new Day[NUM_DAYS]; //build the workweek using the constant number of days in the week
                    for(int i = 0;i<NUM_DAYS;++i)
                    {
                        days_[i].day_ = i;
                    }
                }

                ~Schedule()
                {
                    //delete[] days_;

                }

                Schedule& operator=(const Schedule& rhs){days_ = rhs.days_; return *this;}
                void set_day_schedule(int day, std::string time) //set an employees schedule for a particular day of the week
                {
                    days_[day].day_ = day;
                   //days_[day].set_time(time);
                    days_[day].time_ = time;
                }

                Day get_day_schedule(int day){return days_[day];} //returns the day's schedule

                const int NUM_DAYS = 7; //number of days in the week will always be 7 used to build an array of days
                Day* days_;             //array of days to store a schedule
        };

        //ctor
        PEmployee();
        PEmployee(std::string name, int hours, Schedule schedule = Schedule{}):
            name_(name),hours_(hours),schedule_(schedule){}
        //dtor
        ~PEmployee();
        //operator
        PEmployee& operator=(const PEmployee & rhs){name_ = rhs.name_; hours_ = rhs.hours_; schedule_ = rhs.schedule_; return *this;}

        //get methods
        std::string    get_name            ()        {return name_;    }
        float          get_hours           ()        {return hours_;   }
        Schedule       get_schedule        ()        {return schedule_;}
        std::string    print_day_schedule  (int day);
        std::string    print_schedule      ();

        //set methods
        void     set_day_schedule    (const int& day,const std::string& time) {schedule_.set_day_schedule(day,time);}
        void     set_name            (const std::string & name)               {name_ = name;}
        void     set_hours           (float hours)                            {hours_ = hours;}

        std::string name_;  //employee name
        float hours_;         //employee's pool of hours
        Schedule schedule_; //employee's work schedule

};

#endif // PEMPLOYEE_H
