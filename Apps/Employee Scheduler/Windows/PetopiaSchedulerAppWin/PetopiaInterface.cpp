#include "PetopiaInterface.h"
#include<iostream>
#include<vector>
#include<sstream>
#include <fstream>

/***************************************\
|PETOPIAINTERFACE_CPP                   |
|By:Michael Carey                       |
|Date: Feb/19/2020                      |
|Purpose:class to interact with business|
|employees and schedules                |
\***************************************/

using namespace std;

PetopiaInterface::~PetopiaInterface()
{
    //dtor
}

PetopiaInterface::PetopiaInterface()
{
    open_config();
    open_file(current_path_);
}

void PetopiaInterface::open_config()  //parser method opens the config file for the program and reads in program info
{
    //std::string::size_type sz;   // alias of size_t
    string line,line2;

    ifstream myfile ("Config.txt");
    if (myfile.is_open()) //if we actually were able to open the file
    {
        while ( getline (myfile,line) )
        {
            if(line == "#PWD") //next line will be the value to set in memory
            {
                getline (myfile,line2);
                //petopia_.size_ = std::stoi (line2,&sz);
                petopia_.password_ = line2;

            }
            else if(line == "#PATH")//next line will be the value to set in memory
            {

                 getline (myfile,line2);
                 current_path_ = line2;
                 //vector<string> employee = parse_line(line2); //parse all the info from this employee into memory
                 //parse_employee(employee);
            }

        }
        myfile.close(); //close the configuration file
        status_ = "Config.txt Loaded";
    }
    else //file missing or couldnt be opened
    {
        cout << "Unable to open Config.txt";
        status_ = "Unable to open Config.txt";
    }
}

void PetopiaInterface::open_file(std::string file_path) //method to load a particular file dataset into memory
{
    petopia_.employees_.clear(); //remove previous employees in memory before loading file

    //std::string::size_type sz;   // alias of size_t
    string line,line2;

    ifstream myfile (file_path);
    if (myfile.is_open()) //if we actually were able to open the file
    {
        while ( getline (myfile,line) )
        {
            if(line == "#PWD") //next line will be the value to set in memory
            {
                //getline (myfile,line2);
                //petopia_.size_ = std::stoi (line2,&sz);
                //petopia_.password_ = line2;

            }
            else if(line == "#Employee Info")//next line will be the value to set in memory
            {
                 getline (myfile,line2);
                 vector<string> employee = parse_line(line2); //parse all the info from this employee into memory
                 parse_employee(employee);
            }

        }
        myfile.close(); //close the configuration file
        status_ = "Config.txt Loaded";
    }
    else //file missing or couldnt be opened
    {
        cout << "Unable to open Config.txt";
        status_ = "Unable to open Config.txt";
    }
}

std::vector<std::string> PetopiaInterface::parse_line(std::string str) //returns a vector of strings that contain all the employee attributes
{

   vector<string> result;
   stringstream s_stream(str); //create string stream from the string
   while(s_stream.good())
   {
      string substr;
      getline(s_stream, substr, ','); //get first string delimited by comma
      result.push_back(substr);
   }
    return result;
}

void PetopiaInterface::parse_employee(const vector<string>& employee) //takes in the vector of attributes and sets the value in memory
{
    std::string::size_type sz;   // alias of size_t
    //std::stoi (line2,&sz);
    enum info{Name=0,Hours,Monday,Monday_Hours,Tuesday,Tuesday_Hours,Wednesday,Wednesday_Hours,Thursday,Thursday_Hours,
    Friday,Friday_Hours,Saturday, Saturday_Hours,Sunday,Sunday_Hours};

    //set employee hours
    petopia_.add_employee(employee[Name],std::stoi(employee[Hours],&sz));
    //set schedule for each day of the week
    petopia_.set_employee_day_schedule(employee[Name],"Monday",employee[Monday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Tuesday",employee[Tuesday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Wednesday",employee[Wednesday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Thursday",employee[Thursday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Friday",employee[Friday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Saturday",employee[Saturday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Sunday",employee[Sunday_Hours]);


}
