#include "Petopia.h"
#include "PEmployee.h"
#include <string>
#include <iostream>
#include <utility>

using namespace std;

Petopia::Petopia()
{
    num_employees_ = 10;
    size_ = 0;
}




Petopia::~Petopia()
{

}

void Petopia::init()
{
    employees_.reserve(num_employees_);
}

void Petopia::set_employee_day_schedule(std::string name, string day , string schedule)
{
    int employee_ID = find_employee(name);

    employees_[employee_ID].schedule_.set_day_schedule(day_to_int(day),schedule);

}

int Petopia::day_to_int(std::string day)
{
    std::locale loc;
    int retval = 7; //out of bounds
    if(day == "Monday")
    {
        retval = 0;
    }
    else if(day == "Tuesday")
    {
        retval = 1;
    }
    else if(day == "Wednesday")
    {
        retval = 2;
    }
    else if(day == "Thursday")
    {
        retval = 3;
    }
    else if(day == "Friday")
    {
        retval = 4;
    }
    else if(day == "Saturday")
    {
        retval = 5;
    }
    else if(day == "Sunday")
    {
        retval = 6;
    }

    return retval;
}

int Petopia::find_employee(std::string name)
{
    int retval = 0;
    for(int i = 0;i < size_; ++i)
    {

        if( employees_[i].name_ == name)
        {
            retval = i; //employee id found
        }
    }

    return retval;
}

void Petopia::add_employee(std::string name, int hours)
{
    PEmployee employee(name,hours);
    employees_.push_back(employee);
    ++size_;
}

void Petopia::print_schedule()
{

    for(int i = 0; i < (int)employees_.size() ;++i)
    {
        employees_[i].print_schedule();

    }

}
