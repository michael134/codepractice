#include "PEmployee.h"
#include <string>
#include <iostream>

using namespace std;


PEmployee::PEmployee()
{
    //ctor
}

PEmployee::~PEmployee()
{
    //dtor
}

void PEmployee::print_day_schedule(int day)
{
    cout << schedule_.days_[day].time_;
}

void PEmployee::print_schedule()
{

    cout  << "SCHEDULE:" << get_name() <<endl;
    cout<<"--------------------------------------------------------------------------------------------------\n";
    cout<<"|Monday-------|Tuesday------|Wednesday----|Thursday-----|Friday-------|Saturday-----|Sunday-------|\n";
    cout<< "|";
    for (int j = 0; j < schedule_.NUM_DAYS ;++j)
    {
        print_day_schedule(j);
        cout<< "|";
    }
    cout<< "\n";
    cout<<"--------------------------------------------------------------------------------------------------\n";

}



