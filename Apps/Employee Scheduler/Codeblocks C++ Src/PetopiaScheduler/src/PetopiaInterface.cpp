#include "PetopiaInterface.h"
#include<iostream>
#include<vector>
#include<sstream>
#include <fstream>

using namespace std;

PetopiaInterface::~PetopiaInterface()
{
    //dtor
}

PetopiaInterface::PetopiaInterface(std::string cmd)
{
}

PetopiaInterface::PetopiaInterface()
{
    open_config();

    do
    {
        cout << "Petopia Scheduling Assistant Please Enter Command: ";
        cin >> cmd_;
        if(cmd_ == "P" || cmd_ == "P" ||cmd_ == "Print" ||cmd_ == "print" )
        {
            petopia_.print_schedule();
        }
        else if(cmd_ == "AE" || cmd_ == "ae" ||cmd_ == "AddEmployee" ||cmd_ == "addemployee" )
        {
            string employee_nm, first, last;
            int num_hours;
            cout << "Enter Employee First Name:";
            cin >> first;
            cout << "Enter Employee Last Name:";
            cin >> last;
            employee_nm = first + " " + last;

            cout<<"\n";
            cout << "Enter "<< employee_nm <<"'s Hours:";
            cin >> num_hours;
            cout<<"\n";
            cout<< employee_nm << "Was Added Successfully!\n";

            petopia_.add_employee(employee_nm,num_hours);
        }
        else if(cmd_ == "US" || cmd_ == "us" ||cmd_ == "updateschedule" ||cmd_ == "UpdateSchedule" )
        {
            std::string f_name,l_name,full_name,day_of_week,start_time,end_time,concat_time;
            cout<< "Enter Employee First Name:"; cin>> f_name;
            cout<< "Enter Employee Last Name:"; cin>> l_name;
            cout <<"Enter Day of the Week (Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday):"; cin >>day_of_week;
            cout<<"Enter Start Time (HH:MM):"; cin >>start_time;
            cout<<"Enter End Time (HH:MM):"; cin >>end_time;
            concat_time = start_time + "-"+ end_time;
            full_name = f_name + " " + l_name;
            petopia_.set_employee_day_schedule(full_name,day_of_week,concat_time);
        }
        else if(cmd_ == "H" || cmd_ == "h" ||cmd_ == "Help" ||cmd_ == "help" )
        {
            cout<< "Commands:\n" <<"\tAddEmployee: Adds a New Employee to the Schedule\n"
            <<"\tUpdateSchedule: Updates Employee Schedule for a Particular Day\n"
            <<"\tPrint: Prints the Current Schedule to the Console\n"
            <<"\tQuit: Exits the Program\n";
        }
        else
        {
            cout<< "Command Not Recognized Use Help | help | H | h Command\n";
        }
    }
    while(cmd_ != "Q" && cmd_ != "q" && cmd_ != "Quit" && cmd_ != "quit" );

}


void PetopiaInterface::add_employee()
{

}
void PetopiaInterface::remove_employee()
{


}
void PetopiaInterface::set_day_schedule_person()
{

}
void PetopiaInterface::print_schedule()
{

}
void PetopiaInterface::save()
{

}

void PetopiaInterface::get_employee_id(std::string name)
{

}

void PetopiaInterface::open_config()
{
    std::string::size_type sz;   // alias of size_t
    string line,line2;

    ifstream myfile ("Config.txt");
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {
            if(line == "#Employees")
            {
                getline (myfile,line2);
                petopia_.size_ = std::stoi (line2,&sz);

            }
            else if(line == "#Employee Info")
            {
                 getline (myfile,line2);
                 vector<string> employee = parse_line(line2);
                 parse_employee(employee);
            }

        }
        myfile.close();
    }
    else cout << "Unable to open Config.txt";


}

std::vector<std::string> PetopiaInterface::parse_line(std::string str)
{

   vector<string> result;
   stringstream s_stream(str); //create string stream from the string
   while(s_stream.good())
   {
      string substr;
      getline(s_stream, substr, ','); //get first string delimited by comma
      result.push_back(substr);
   }
    return result;
}

void PetopiaInterface::parse_employee(const vector<string>& employee)
{
    std::string::size_type sz;   // alias of size_t
    //std::stoi (line2,&sz);
    enum info{Name=0,Hours,Monday,Monday_Hours,Tuesday,Tuesday_Hours,Wednesday,Wednesday_Hours,Thursday,Thursday_Hours,
    Friday,Friday_Hours,Saturday, Saturday_Hours,Sunday,Sunday_Hours};

    petopia_.add_employee(employee[Name],std::stoi(employee[Hours],&sz));

    petopia_.set_employee_day_schedule(employee[Name],"Monday",employee[Monday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Tuesday",employee[Tuesday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Wednesday",employee[Wednesday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Thursday",employee[Thursday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Friday",employee[Friday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Saturday",employee[Saturday_Hours]);
    petopia_.set_employee_day_schedule(employee[Name],"Sunday",employee[Sunday_Hours]);


}
