#ifndef PETOPIA_H
#define PETOPIA_H
#include "PEmployee.h"
#include <string>
#include <vector>



class Petopia
{
    public:
        Petopia();
        ~Petopia();

        void init();
        int size(){return size_;}
        void set_employee_day_schedule(std::string name, std::string day , std::string schedule);
        void add_employee(std::string name, int hours);
        void remove_employee(int index){employees_.erase(employees_.begin() + index); --size_;}
        int day_to_int(std::string day);
        int find_employee(std::string name);
        void print_schedule();

        int num_employees_;
        int size_; //current amount of employees
        std::vector<PEmployee> employees_;

};

#endif // PETOPIA_H
