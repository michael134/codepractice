#ifndef PETOPIAINTERFACE_H
#define PETOPIAINTERFACE_H
#include "Petopia.h"
#include<string>


class PetopiaInterface
{
    public:
        PetopiaInterface();
        PetopiaInterface(std::string cmd);
        ~PetopiaInterface();

    void add_employee();
    void remove_employee();
    void set_day_schedule_person();
    void print_schedule();
    void save();
    void open_config();
    void parse_employee(const std::vector<std::string>& employee);
    std::vector<std::string> parse_line(std::string str);

    void get_employee_id(std::string name);

    private:
    Petopia petopia_;
    std::string cmd_;
};

#endif // PETOPIAINTERFACE_H
